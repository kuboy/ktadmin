<?php 
// +----------------------------------------------------------------------
// | 狂团[kt8.cn]旗下KtAdmin是为独立版SAAS系统而生的快速开发框架.
// +----------------------------------------------------------------------
// | [KtAdmin] Copyright (c) 2022 http://ktadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

namespace app\base\model\admin\system;
use think\facade\Db;
use think\facade\Session;
use app\base\model\BaseModel;

/* 
* 接口配置
*/
class SetModel extends BaseModel
{
    /**
    * Baidu AI配置  获取
    */
    static public function BaiduAi()
    {
        $uid = Session::get('uid');
        $res = Db::table("kt_base_baiduai_config")->field("appid,apikey,secretkey")->where('uid',$uid)->find();
        return success('百度AI配置',$res);
    }

    /**
    * Baidu AI配置  保存
    */
    static public function BaiduAiSet($req)
    {
        $uid = Session::get('uid');
        $data = [];
        $data['appid'] = $req->param('appid');
        if(!$data['appid']) return error('请输入Appid');
        $data['apikey'] = $req->param('apikey');
        if(!$data['apikey']) return error('请输入Key');
        $data['secretkey'] = $req->param('secretkey');
        if(!$data['secretkey']) return error('请输入Secret');
        $is = Db::table("kt_base_baiduai_config")->where('uid',$uid)->find();
        if($is){
            $data['id'] = $is['id'];
        }
        $data['uid'] = $uid;
        $res = Db::table("kt_base_baiduai_config")->save($data);
        return success("保存成功");
        
    }
    /**
    * Aliyun 语音合成配置  获取
    */
    static public function Aliyun()
    {
        $uid = Session::get('uid');
        $res = Db::table("kt_base_aliai_config")->field("accesskey_id,accesskey_secret,region")->where('uid',$uid)->find();
        return success('阿里云AI配置',$res);
    }

    /**
    * Aliyun 语音合成配置  保存
    */
    static public function AliyunSet($req)
    {
        $uid = Session::get('uid');
        $data = [];
        $data['region'] = $req->param('region');
        $data['accesskey_id'] = $req->param('accesskey_id');
        if(!$data['accesskey_id']) return error('请输入AccessKey Id');
        $data['accesskey_secret'] = $req->param('accesskey_secret');
        if(!$data['accesskey_secret']) return error('请输入AccessKey Secret');
        $is = Db::table("kt_base_aliai_config")->where('uid',$uid)->find();
        if($is){
            $data['id'] = $is['id'];
        }
        $data['uid'] = $uid;
        $res = Db::table("kt_base_aliai_config")->save($data);
        return success("保存成功");
    }

    /**
    * 腾讯云 语音转合字幕配置  获取
    */
    static public function Tencent()
    {
        $uid = Session::get('uid');
        $res = Db::table("kt_base_tencentai_config")->field("secret_id,secret_key")->where('uid',$uid)->find();
        return success('腾讯云AI配置',$res);
    }

    /**
    * 腾讯云 语音转合字幕配置  保存
    */
    static public function TencentSet($req)
    {
        $uid = Session::get('uid');
        $data = [];
        $data['secret_id'] = $req->param('secret_id');
        if(!$data['secret_id']) return error('请输入SecretId');
        $data['secret_key'] = $req->param('secret_key');
        if(!$data['secret_key']) return error('请输入SecretKey');
        $is = Db::table("kt_base_tencentai_config")->where('uid',$uid)->find();
        if($is){
            $data['id'] = $is['id'];
        }
        $data['uid'] = $uid;
        $res = Db::table("kt_base_tencentai_config")->save($data);
        return success("保存成功");
    }

    /**
    * GPT配置  获取
    */
    static public function Gpt()
    {
        $uid = Session::get('uid');
        $res = Db::table("kt_base_gpt_config")->field('channel,openai,api2d,wxyy,tyqw,kltg,chatglm')->json(['openai','api2d','wxyy','tyqw','kltg','chatglm'])->where('uid',$uid)->find();
        return success('GPT配置',$res);
    }

    /**
    * GPT配置  保存
    */
    static public function GptSet($req)
    {
        $uid = Session::get('uid');
        $data = [];
        $data['channel'] = $req->param('channel');
        $data['openai'] = $req->param('openai',[]); 
        $data['api2d'] = $req->param('api2d',[]);
        $data['wxyy'] = $req->param('wxyy',[]);
        $data['tyqw'] = $req->param('tyqw',[]);
        $data['kltg'] = $req->param('kltg',[]);
        $data['chatglm'] = $req->param('chatglm',[]);
        switch ($data['channel']) {
            case '1':
                if(!$data['openai']['api_key'])  return error('请输入apikey');
                break;
            case '2':
                if(!$data['api2d']['forward_key'])  return error('请输入forward_key');
                break;
            case '3':
                if(!$data['wxyy']['key'])  return error('请输入key');
                if(!$data['wxyy']['secret'])  return error('请输入secret');
                break;
             case '4':
                break;
             case '5':
                break;
            case '6':
                if(!$data['wxyy']['api_key'])  return error('请输入api_key');
                if(!$data['wxyy']['public_key'])  return error('请输入public_key');
                break;
            
        }

        $is = Db::table("kt_base_gpt_config")->where('uid',$uid)->find();
        if($is){
            $data['id'] = $is['id'];
            $data['u_time'] = date('Y-m-d H:i:s');
        }else{
            $data['c_time'] = date('Y-m-d H:i:s');
            $data['u_time'] = date('Y-m-d H:i:s');
        }
        $data['uid'] = $uid;
        $res = Db::table("kt_base_gpt_config")->json(['openai','api2d','wxyy','chatglm','tyqw','kltg'])->save($data);
        return success("保存成功");
    }
}