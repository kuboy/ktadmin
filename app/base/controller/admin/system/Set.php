<?php
// +----------------------------------------------------------------------
// | 狂团[kt8.cn]旗下KtAdmin是为独立版SAAS系统而生的快速开发框架.
// +----------------------------------------------------------------------
// | [KtAdmin] Copyright (c) 2022 http://ktadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

namespace app\base\controller\admin\system;

use think\facade\Db;
use think\facade\Session;
use app\base\controller\BaseAdmin;
use app\base\model\admin\system\SetModel;

/**
 * 接口配置
 */
class Set extends BaseAdmin
{
	
	/**
    * Baidu AI配置  获取
    */
    public function BaiduAi()
    {
        return SetModel::BaiduAi();
    }

    /**
    * Baidu AI配置  保存
    */
    public function BaiduAiSet()
    {
        return SetModel::BaiduAiSet($this->req);
    }
    /**
    * Aliyun 语音合成配置  获取
    */
    public function Aliyun()
    {
        return SetModel::Aliyun();
    }

    /**
    * Aliyun 语音合成配置  保存
    */
    public function AliyunSet()
    {
        return SetModel::AliyunSet($this->req);
    }

	/**
    * 腾讯云 语音转合字幕配置  获取
    */
    public function Tencent()
    {
        return SetModel::Tencent();
    }

    /**
    * 腾讯云 语音转合字幕配置  保存
    */
    public function TencentSet()
    {
        return SetModel::TencentSet($this->req);
    }

    /**
    * GPT配置  获取
    */
    public function Gpt()
    {
        return SetModel::Gpt();
    }

    /**
    * GPT配置  保存
    */
    public function GptSet()
    {
        return SetModel::GptSet($this->req);
    }
}