<?php

namespace app\chat\controller;

use app\BaseController;
use think\Request;

class Base extends BaseController
{
    protected $req;

    public function  __construct(Request $request){
        $this->req = $request;
    }
}